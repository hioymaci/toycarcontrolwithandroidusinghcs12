/* CSE 334 MICROPROCESSORS */
/* author: Halil Ibrahim Oymaci - student ID: 121044019 */
/* since: 01.06.2016 */
/* Project Description: Toy Car by controlling a machine that has bluetooth module */
/* Dragonboard12 that has HCS12 microprocessor communicates bluetooth module and controls motors which is belong to toy car */

/* PIN CONFIGURATION:
 *
 * back-motor: forward = PORTA_BIT0
 * back motor: back    = PORTA_BIT1
 * front-motor: right  = PORTA_BIT2
 * front-motor: left   = PORTA_BIT3
 * motor-driver ground = Vssx
 * HC-06 bluetooth module Vcc = Vddx
 * HC-06 bluetooth module Rx = Ps3 using voltage-divisor
 * HC-06 bluetooth module Tx = Ps2
 * HC-06 bluetooth module ground = Vssx
 * PORTH_BIT0 = PORTT_BIT0 - horn
 * PORTH_BIT1 = PORTT_BIT1 - increase pwm delay
 * PORTH_BIT2 = PORTT_BIT2 - decrease pwm delay
 * PORTH_BIT3 = PORTT_BIT3 - forward motor with pwm
 * 
 * PORTH_BIT6 - enable pwm delay for back-motors 
 * PORTH_BIT7 - enable 7-segment for showing pwm delay / disable showing received data on PORTB
 */

/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
@ 	author_signature:				_			   @
@								   �_�			   @
@	 _	    _   _-_-_-_	  _			_   _		   @
@	� �	   � � � �	 � � � �	   � � � �		   @
@	� �	   � � � �   � � � �	   � � � �		   @
@   � �_-_-� � � �_-_� � � �	   � � � �		   @
@   � �	   � � � �	 � � � �_-_-_  � � � �_-_-_	   @
@   �_�	   �_� �_�	 �_� �_-_-_-_� �_� �_-_-_-_�   @
@												   @
@ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */

#ifndef FINAL_PROJECT_H
#define FINAL_PROJECT_H

#include <hidef.h>      			/* common defines and macros */
#include "derivative.h"      		/* derivative-specific definitions */
#include <stdio.h>					/* for using function sprintf*/

//#ifdef DEBUG						// enable debug mode
//#define ECHO						// send received data to transmitter again
#define TOVF						// enable timer overflow
//#define SERIAL0					// use serial0 to communication
#define LED							// enable PORTB for knight-rider
#define LCD		  					// use LCD on Dragonboard12
#define SERIAL1						// use serial1 to communication
#define SEVEN_SEGMENT				// use seven-segment
#define MAX_PWM	30					// maximum delay ms for pulse-width modulation
#define	MIN_PWM 2					// minimum delay ms for pulse-width modulation

// these 6 macros and related functions are quoted in 013504880X_pp12a_alp.pdf on gtu moodle on 2016 Spring Term
#define LCD_DAT PORTK 				/* Port K drives LCD data pins, E, and RS */
#define LCD_DIR DDRK 				/* Direction of LCD port */
#define LCD_E 0x02 					/* LCD E signal */
#define LCD_RS 0x01 				/* LCD Register Select signal */
#define CMD 0 						/* Command type for put2lcd */
#define DATA 1 						/* Data type for put2lcd */

enum Direction {FORWARD,BACKWARD};  /* direction for knight-rider */

void setupLCD(void); 				/* Initialize LCD display */
void sentToLCD(char c, char type); 	/* Write command or data to LCD controller */
void writeStringToLCD (char *ptr); 	/* Write a string to the LCD display */
void delay_50us(int n); 			/* Delay n 50 microsecond intervals */
void delay_1ms(int n) ; 			/* Delay n 1 millisecond intervals */
void delay_second(int n);			/* Delay n 1 milisecond using timeroverflow interrupt */

void project(void);					/* Set serialcom, timer, DDRs, ports registers and LCD, then received data and process infitinely*/

void soundTheHorn(void);			/* sound the horn on buzzer */

void showPwmOn7Segment(void);		/* show pwm number on 7-segment along 1 second */

void knightRiderOnPortB(unsigned char); /* show knight rider giving direction on PORTB */

/* This function runs the related motor on toy car according to argument that is received from bluetooth module
   PORTA is connected to toy car motors

  Controlling keys:
  	 
  	  ^
  	  | 		q:  a & w	,	+: increase pwm
      w 		e:  d & w	,	-: decrease pwm
 <- a   d ->    z:  a & s 	,	*: stop
  	  s			c:  d & s	,   h: horn
  	  |
  	  
*/
void processData(unsigned char data);

#endif