#include "finalproject.h"


unsigned int tim_ovf_count = 0;		// store number of timer overflow
unsigned int pwm_number = MAX_PWM; 		// pulse-width modulation value
																			
/* This array store digits on seven segment according to this structure:
*    a
*    _			 bit-order:	 7 6 5 4 3 2 1 0
* f |_| b		 PORTB  ==>  0 g f e d c b a
* e |_| c		 g is the middle line										 
*
*    d
*/
#ifdef SEVEN_SEGMENT					
unsigned char digitsOn7Segment[10]={0b00111111,0b00000110,0b01011011,0b01001111,0b01100110,0b01101101,0b01111101,0x07,127,0b01101111};
#endif

void project(void){

	unsigned char rData;
	
  	#ifdef SERIAL1
	SCI1BDH = 0;    	// set baud rate to high byte
	SCI1BDL = 0x9C; 	// set baud rate 9600 for xtal= 48 MHz
	SCI1CR1 = 0;	  	// 8-bit data, no parity
	SCI1CR2 = 0x0C;		// enable transmit and receiver
	#endif
	
	#ifdef SERIAL0
	SCI0BDH = 0;    	// set baud rate to high byte
	SCI0BDL = 0x9C; 	// set baud rate 9600 for xtal= 48 MHz
	SCI0CR1 = 0;	  	// 8-bit data, no parity
	SCI0CR2 = 0x0C;		// enable transmit and receiver
	#endif
	
	DDRB = 0xFF;	  	// make PORTB output
	DDRP = 0xFF;  		// make PORTP output for 7-segment  
	DDRA = 0xFF; 	  	// make PORTA output for toy car motors
	DDRH = 0;		    // make PORTH input for DIP switch
	DDRT &= ~0x0F;		// make PT0-1-2-3 input for input capture
	DDRT |= 0x20;	  	// make PT5 output for buzzer   
	
	// clear registers     1010101010
	PORTA = 0;
	PORTB = 0;
	
	TSCR1 = 0x80;   	// timer enable
	
	#ifdef TOVF
	TSCR2 = 0x80;   	// timer overflow interrupt enable and prescaler = 0;
	#endif
	
	#ifndef TOVF
	TSCR2 = 0x00;		// disable tovf interrupt, prescaler = 0
	#endif
	
	TIOS &= ~0x0F;		// select channel0-1-2-3 for input capture
	TCTL4 = 0xAA;		// select ch0-1-2-3 for capture on falling edge
	
	TFLG1 |= 0x0F;		// clear ch0-1-2-3 flag
	
	TIE |= 0x0F;		// enable interrupt for ch0-1-2-3
	
	__asm CLI;			// clear global interrupt flag
		
	setupLCD();
	
	#ifdef LCD
	writeStringToLCD("W E L C O M E");
	#endif

	// infitine loop
	for(;;) {
		#ifdef SERIAL1
		
		// polling is especially used here because of processData function is long function and interrupt routine should be small
	  	while(!(SCI1SR1 & SCI1SR1_RDRF_MASK)); // wait data is received 
	  	rData = SCI1DRL; // load received data to variable,this is automati clear RDRF flag
	  
	  	#ifdef ECHO
	  	while(!(SCI1SR1 & SCI1SR1_TDRE_MASK)); // wait data is transmitted
	  	SCI1DRL = rData; // send data,this is automati clear TDRE flag
	    #endif
	    
	  	processData(rData);
	  	#endif
	  	
		#ifdef SERIAL0
		while(!(SCI0SR1 & SCI0SR1_RDRF_MASK)); // wait data is received
	  	rData = SCI0DRL; // load received data to variable,this is automati clear RDRF flag
	  	#ifdef ECHO
	  	while(!(SCI0SR1 & SCI0SR1_TDRE_MASK)); // wait data is transmitted
	  	SCI0DRL = rData; // send data,this is automati clear TDRE flag
	  	#endif
	  	processData(rData);
	  	#endif
	}
	
}

void processData(unsigned char data) {
	
	char string[20];
	int temp;
	
	// show data as binary on PORTB if dipswitch7 is low
	if(PTH_PTH7 == 0)
		PORTB = data;
	
	if(data == 'w') // move forward
	{
		PORTA = 0x01;
		
		if(PTH_PTH6 == 1) {	
			delay_1ms(pwm_number);
			PORTA = 0;
		}
		
		#ifdef LCD
		writeStringToLCD("forward");
		#endif
	}
	else if(data == 's') // move back
	{
		PORTA = 0x02;
		
		if(PTH_PTH6 == 1) {	
			delay_1ms(pwm_number);
			PORTA = 0;
		}
		
		#ifdef LCD
		writeStringToLCD("back");
		#endif
	}
	else if(data == 'd') // move right
	{
		PORTA = 0x04;
		
		#ifdef LCD
		writeStringToLCD("right");
		#endif
	}
	else if(data == 'a') // move left
	{
		PORTA = 0x08;
		
		#ifdef LCD
		writeStringToLCD("left");
		#endif
	}
	else if(data=='*' || data==' ') {
		PORTA=0;
	  	#ifdef LCD
		writeStringToLCD("stop");
		#endif
	}
	else if(data == 'e') // move forward and right
	{
		PORTA = 0x05;
		
		if(PTH_PTH6 == 1) {	
			delay_1ms(pwm_number);
			PORTA &= ~0x01;
		}
		
		#ifdef LCD
		writeStringToLCD("right and forward");
		#endif
	}
	else if(data == 'q') // move forward and left
	{
		PORTA = 0x09;
		
		if(PTH_PTH6 == 1) {	
			delay_1ms(pwm_number);
			PORTA &= ~0x01;
		}
		
		#ifdef LCD
		writeStringToLCD("left and forward");
		#endif
	}
	else if(data == 'z') // move back and left
	{
		PORTA = 0x0A;
		
		if(PTH_PTH6 == 1) {	
			delay_1ms(pwm_number);
			PORTA &= ~0x02;
		}
			
		#ifdef LCD
		writeStringToLCD("left and back");
		#endif
	}
	else if(data == 'c') // move back and right
	{
		PORTA = 0x06;
		
		if(PTH_PTH6 == 1) {	
			delay_1ms(pwm_number);
			PORTA &= ~0x02;
		}
		
		#ifdef LCD
		writeStringToLCD("right and back");
		#endif
	}
	else if(data == 'h') // sound the horn on buzzer
	{				  
		#ifdef LCD
		writeStringToLCD("horn");
		#endif
		soundTheHorn();
	}
	else if(data =='+')    // increase pulse-width modulation value
	{
		if(pwm_number<MAX_PWM)
			++pwm_number; 
	} 
	else if(data=='-')    // decrease pulse-with modulation value
	{
	  	if(pwm_number>MIN_PWM)
			--pwm_number;
	} 
	else // unknown data
	{
		temp = sprintf(string,"unknown command:%c",data);
		// aim of storing sprintf return value is that codewarrior gives warning
		
		#ifdef LCD
		writeStringToLCD(string);
		#endif
	}
}

void setupLCD(void)
{
	LCD_DIR = 0xFF; 		/* configure LCD_DAT port for output */
  	delay_1ms(100); 		/* Wait for LCD to be ready */
  	sentToLCD(0x28, CMD); 	/* set 4-bit data, 2-line display, 5x7 font */
  	sentToLCD(0x0F, CMD); 	/* turn on display, cursor, blinking */
  	sentToLCD(0x06, CMD); 	/* move cursor right */
  	sentToLCD(0x01, CMD); 	/* clear screen, move cursor to home */
  	delay_1ms(2); 			/* wait until "clear display" command complete */
}

void writeStringToLCD (char *str)
{
	sentToLCD(0x01, CMD); 		/* clear screen, move cursor to home */
	delay_1ms(2); 				/* wait until "clear display" command complete */
	while (*str) { 				/* While character to send */
    	sentToLCD(*str, DATA); 	/* Write data to LCD */
    	delay_50us(1); 			/* Wait for data to be written */
    	str++; 					/* Go to next character */
	} 
}

void sentToLCD(char c, char type) {
  char c_lo, c_hi;
  
  c_hi = (c & 0xF0) >> 2; 	/* Upper 4 bits of c */
  c_lo = (c & 0x0F) << 2; 	/* Lower 4 bits of c */
  
  /*hsb nibble*/
  LCD_DAT &= (~LCD_RS); 	/* select LCD command register */
  if (type == DATA)
    LCD_DAT = c_hi | LCD_RS; /* output upper 4 bits, E, RS high */
  else 
    LCD_DAT = c_hi; 		/* output upper 4 bits, E, RS low */
    
  LCD_DAT |= LCD_E; 		/* pull E signal to high */
  __asm(nop); 				/* 246 ns high voltage */
  __asm(nop);
  __asm(nop);
  LCD_DAT &= (~LCD_E); 		/* pull E to low */
  
  /*lsb nibble*/
  LCD_DAT &= (~LCD_RS); 	/* select LCD command register */
  if (type == DATA)
    LCD_DAT = c_lo | LCD_RS;/* output lower 4 bits, E, RS high */
  else 
    LCD_DAT = c_lo; 		/* output lower 4 bits, E, RS low */
  LCD_DAT |= LCD_E; 		/* pull E to high */
  __asm(nop); 				/* 246 ns high voltage */
  __asm(nop);
  __asm(nop);
  LCD_DAT &= (~LCD_E); 		/*pull E to low */
  
  delay_50us(1); 			/* Wait for command to execute */ 
}

void delay_50us(int n) 		/* Delay n 50 microsecond intervals */
{
  unsigned int a,b;
  
  for(a=0; a<n; ++a)
    for(b=0; b<200; ++b);  
}


void delay_1ms(int n) 		/* Delay n 1 millisecond intervals */
{
  unsigned int a,b;
  
  for(a=0; a<n; ++a)
    for(b=0; b<4000; ++b);
}

void delay_second(int n) 		/* Delay n 1 milisecond using timeroverflow interrupt */
{
	unsigned int temp = tim_ovf_count;
	
	temp += 366*n;				// every 366 overflow equal to 1 second for xtal = 48 MHz
	
	while(temp!=tim_ovf_count); // wait 1 ms	
}

void soundTheHorn(void) {
	int i,j;
	
	// sound on buzzer along 1 second
	
	for(j=0;j<5;++j){
	  
  		for(i=0;i<10;++i) {
  			PTT |= 0x20;
  			delay_1ms(1);
  			PTT &= ~0x20;
  			delay_1ms(1);	
  		}
  		
  		for(i=0;i<10;++i) {
  			PTT |= 0x20;
  			delay_1ms(2);
  			PTT &= ~0x20;
  			delay_1ms(1);	
  		}
	}
}

// show pwm number on 7-segment along 1 second
void showPwmOn7Segment(void) {
	
	int i,digit1,digit2;
	
	digit1 = pwm_number%10;
	digit2 = (pwm_number/10)%10;	 
	 	
	#ifdef SEVEN_SEGMENT
	for(i=0;i<450;++i) {
		PTP = 0x07;
		PORTB = digitsOn7Segment[digit1];
		delay_1ms(1);
		PTP = 0x0D;
		PORTB = digitsOn7Segment[digit2];
		delay_1ms(1);
	}
	PORTB = 0;
	PTP = 0;
	#endif	
	
}

// show knight-rider according to givind direction on PORTB along about 1 second
void knightRiderOnPortB(unsigned char d) {

	if(d==FORWARD)
	{
		PORTB = 0x01;
		while(PORTB_BIT7 != 1) {
			PORTB = PORTB << 1;
			delay_1ms(25);
		}
		PORTB = 0;
	} else if ( d== BACKWARD) 
	{
		PORTB = 0x80;
		while(PORTB_BIT0 != 1) {
			PORTB = PORTB >> 1;
			delay_1ms(25);
		}
		PORTB = 0;
	}
	
}


/* ########################### INTERRUPT SUBROUTINES ########################### */

// show PWM number on 7-segment every timeroverflow if pth7 is high
interrupt ((0x10000-Vtimovf)/2-1) void TIMOVF_ISR(void){
	
	#ifdef SEVEN_SEGMENT
	int digit1,digit2;
	
	if(PTH_PTH7 == 1 ) {
	
		digit1 = pwm_number%10;
		digit2 = (pwm_number/10)%10;
		
		PTP = 0x07;
		PORTB = digitsOn7Segment[digit1];
		delay_1ms(1);
		PTP = 0x0B;
		PORTB = digitsOn7Segment[digit2];
		delay_1ms(1);
		PORTB = 0;
		PTP = 0;
	}
	#endif	
	
	TFLG2 |= 0x80; // clear timer overflow flag

}

// sound the horn on buzzer
interrupt ((0x10000-Vtimch0)/2-1) void CH0_ISR(void){

	#ifdef LCD
	writeStringToLCD("horn");
	#endif

	soundTheHorn();
	
	TFLG1 |= 0x01; // clear ch0 flag	
}

// increase pwm number
interrupt ((0x10000-Vtimch1)/2-1) void CH1_ISR(void){
	
	if(pwm_number<MAX_PWM)
	  	pwm_number+=1;
	  
	TFLG1 |= 0x02; // clear ch1 flag	
}

// decrease pwm number
interrupt ((0x10000-Vtimch2)/2-1) void CH2_ISR(void){
	
	if(pwm_number>MIN_PWM)
		pwm_number-=1;
  
	TFLG1 |= 0x04; // clear ch2 flag	
}

// enable forward motor during dipswitch3 is down
interrupt ((0x10000-Vtimch3)/2-1) void CH3_ISR(void){

	#ifdef LCD
	writeStringToLCD("forward");
	#endif

	while(PTH_PTH3==0) {
		PORTA = 0x01;
		delay_1ms(pwm_number);
		PORTA = 0;
		delay_1ms(pwm_number);
	};
	
	TFLG1 |= 0x08; // clear ch2 flag	
}

/* END OF MAIN.C */

/* @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @
@ 	author_signature:			   	_			   @
@								   �_�			   @
@	 _      _   _-_-_-_	  _	       	_   _		   @
@ 	� �	   � � � �	 � � � �	   � � � �		   @
@	� �	   � � � �   � � � �	   � � � �		   @
@   � �_-_-� � � �_-_� � � �	   � � � �		   @
@   � �	   � � � �	 � � � �_-_-_  � � � �_-_-_	   @
@   �_�	   �_� �_�	 �_� �_-_-_-_� �_� �_-_-_-_�   @
@		                                           @
@ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ @ */
